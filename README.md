*****************************************
**libfmm is a 2D Fast Multipole library**
=========================================
*****************************************

* S. Engblom 2011-06-29 (original development)
* A. Goude (extension to GPU's)
* A. Palha (pure C++ interface)


License
=======

For a license statement and additional code, see:
http://user.it.uu.se/~stefane/freeware


Build Instructions
==================

Build fmm library
-----------------
	
	cd path_to_src/src
	python install.py build

Automatically builds c/c++ and python libraries for cpu and gpu versions. Built libraries are stored in path_to_src/build

Example files
-------------

c_cpp
	
	cp path_to_src/build/c_cpp/libfmm2d_cpu.so path_to_src/examples/c_cpp
	cp path_to_src/build/c_cpp/libfmm2d_gpu.so path_to_src/examples/c_cpp
	cd path_to_src/examples/c_cpp
	make all
	./ex01_cpu
	./ex01_gpu

python

	cp path_to_src/build/python/_fmm2d_py_cpu.so path_to_src/examples/python
	cp path_to_src/build/python/_fmm2d_py_gpu.so path_to_src/examples/python
	cd path_to_src/examples/python
	python ex01.py
	
	




