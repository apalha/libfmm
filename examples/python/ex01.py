# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 14:29:09 2013

@author: gorkiana
"""

import numpy
import pyublas
import fmm_velocity # must be imported after pyublas
import time

#------------------------------------------------------------------------------
# Generate exact vorticity and blobs in a regular grid

# the vorticity
w_exact = lambda x,y,gamma,pVortex,epsilon: gamma*(numpy.exp(-(((x-pVortex[0])**2)\
                                            +((y-pVortex[1])**2))/(2.0*epsilon*epsilon)))/(2.0*numpy.pi*epsilon*epsilon)

# the parameters of the vorticity
gamma = 50.0
epsilon = 0.5*numpy.sqrt(numpy.pi/12.0)
pVortex = [0.0, 0.0]

# generate the blobs evenly spaced
nBlobs = 200
overlap = 0.5

x = numpy.linspace(-3.0,3.0,nBlobs)
y = x.copy()

h = x[1]-x[0] # compute the size of the blobs

sigma = h/overlap # compute the size of the blobs

[xBlob,yBlob] = numpy.meshgrid(x,y) # compute the coordinates of all the blobs in a regular grid

xBlob = xBlob.T.flatten().copy() # flatten them out (the .T for transpose is used because we wish to compare with Matlab results)
yBlob = yBlob.T.flatten().copy()

wBlob = w_exact(xBlob,yBlob,gamma,pVortex,epsilon)*h*h

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Compute the induced velocities

# convert blob parameters into the parameters of the fmm solver
alpha = 1.2564312086261696770
cutoff = None
k = 2.0
Ndirect = 35
tol = 1.0e-6

xTarget = xBlob.copy()
yTarget = yBlob.copy()

start = time.time()
vx_fmm_cpu,vy_fmm_cpu = fmm_velocity.fmm_velocity(xBlob,yBlob,wBlob,sigma,k=k,xTarget=xTarget,yTarget=yTarget,Ndirect=Ndirect,tol=tol,cutoff=cutoff,gpuFlag=False)
print "time to compute cpu fmm computation :: " + str(time.time()-start)

start = time.time()
vx_fmm_gpu,vy_fmm_gpu = fmm_velocity.fmm_velocity(xBlob,yBlob,wBlob,sigma,k=k,xTarget=xTarget,yTarget=yTarget,Ndirect=Ndirect,tol=tol,cutoff=cutoff,gpuFlag=True)
print "time to compute gpu fmm computation :: " + str(time.time()-start)

#------------------------------------------------------------------------------











