import numpy
from _fmm2d_py_cpu import fmm2d_py as fmm2d_cpu
from _fmm2d_py_gpu import fmm2d_py as fmm2d_gpu

def fmm_velocity(xBlob,yBlob,wBlob,sigma,k=2.0,xTarget=None,yTarget=None,Ndirect=35,tol=1.0e-6,cutoff=None,gpuFlag=True):

    # convert blob parameters into the parameters of the fmm solver
    alpha = 1.2564312086261696770
    
    ksigmasqr = k*sigma*sigma
    xopt = numpy.sqrt(alpha*ksigmasqr)
    if cutoff==None: cutoff = 5.0*xopt

    if xTarget==None:
        xTarget = xBlob.copy()
        yTarget = yBlob.copy()

    if gpuFlag:
        vy_fmm,vx_fmm = fmm2d_gpu(xBlob,yBlob,wBlob,xTarget,yTarget,Ndirect,xopt,cutoff,tol)
    else:
        vy_fmm,vx_fmm = fmm2d_cpu(xBlob,yBlob,wBlob,xTarget,yTarget,Ndirect,xopt,cutoff,tol)
    
    return -vx_fmm/(2.0*numpy.pi),-vy_fmm/(2.0*numpy.pi)
