#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <math.h>   //C standard basic mathematical operations
#include <stdlib.h>  //C standard general utilities library
#include <stdio.h>
#include <string.h> //C standard constant and function declaration library
#include <float.h>
using namespace std;

#include "fmm2d.h"

int main(){
    /*
        Example function that uses libfmm2d to compute induced velocities in 2D
        using an FMM implementation
    */

    int nBlobs = 6;
    int nTargets = 5;

    // generate some ad hoc arrays to fill in the vectors for the blob data
    double xBlobCoordinates[] = {-0.1, 0.0, 0.1, 9.9, 10.0, 10.1};
    double yBlobCoordinates[] = {0.0, 0.0, 0.0, 10.0, 10.0, 10.0};
    double circulations[] = {-1.0, 1.0, 2.0, -2.0, 3.0, -0.5};

    // generate some ad hoc arrays to fill in the vectors for the xTarget data
    double xTargetCoordinates[] = {2.0, 3.0, 4.0, 3.0, 4.0};
    double yTargetCoordinates[] = {5.0, 4.0, 3.0, 6.0, 2.0};

    // define and fill the blob vectors with the data
    std::vector<double> xBlob(xBlobCoordinates, xBlobCoordinates + sizeof(xBlobCoordinates)/sizeof(double));
    std::vector<double> yBlob(yBlobCoordinates, yBlobCoordinates + sizeof(yBlobCoordinates)/sizeof(double));
    std::vector<double> wBlob(circulations, circulations + sizeof(circulations)/sizeof(double));

    // define and fill the target vectors with the data
    std::vector<double> xTarget(xTargetCoordinates, xTargetCoordinates + sizeof(xTargetCoordinates)/sizeof(double));
    std::vector<double> yTarget(yTargetCoordinates, yTargetCoordinates + sizeof(yTargetCoordinates)/sizeof(double));

    // define the induced velocity vectors
    std::vector<double> vx(nTargets), vy(nTargets);
    std::fill(vx.begin(),vx.end(),0.0f); // make sure they are set to zero
    std::fill(vy.begin(),vy.end(),0.0f);

    // definitions for Oseen vortex blobs of k=2 and sigma = 0.05
    // alpha is an internal constant
    SMOOTHER smooth = OSEEN; // Oseen vortex
    double alpha = 1.2564312086261696770;
    double k = 2.0;
    double sigma = 0.05;
    double ksigmasqr = k*sigma*sigma;
    double xopt = alpha*ksigmasqr;

    // definitions for the FMM solver
    int Ndirect = 35;
    double tol = 1.0e-6;
    double cutoff = 5.0*xopt;

    // call fmm function to compute induced velocities
    fmm2d(xBlob, yBlob,wBlob,xTarget,yTarget,Ndirect,xopt,cutoff,tol,smooth,    // the inputs end here
          vx,vy); // the outputs start here

    // print the computed velocities

    std::cout << "The x component of V :: ";

    for (std::vector<double>::iterator it = vx.begin(); it != vx.end(); ++it)
        std::cout << ' ' << *it;

    std::cout << '\n';

    std::cout << "The y component of V :: ";

    for (std::vector<double>::iterator it = vy.begin(); it != vy.end(); ++it)
        std::cout << ' ' << *it;

    std::cout << '\n';

    return 0;
}


