

typedef enum {SCULLY = 0,RANKINE = 1,OSEEN = 2,DIRAC = 3,
              ERR_SMOOTHER = -1} SMOOTHER;


void fmm2d(std::vector<double>&, std::vector<double>&, std::vector<double>&,
                          std::vector<double>&, std::vector<double>&,
                          int, double, double, double, SMOOTHER,    // the inputs end here
                          std::vector<double>&, std::vector<double>&);
