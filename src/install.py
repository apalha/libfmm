import commands
import os
import sys
import shutil
import time

# generate an ID for the current build to use as a name for the log file
time_stamp = time.localtime()
build_ID = '%d.%02d.%02d__%02d.%02d.%02d' % (time_stamp.tm_year,time_stamp.tm_mon,
                                           time_stamp.tm_mday,time_stamp.tm_hour,
                                           time_stamp.tm_min,time_stamp.tm_sec)

# check the inputs
input_read = True
input_n = 1

while input_read and len(sys.argv)>1: # if no inputs are given, skip input read
    if sys.argv[input_n] == 'clean': # delete any previously installed version (no installation is performed)
        cleanFlag = True
        buildFlag = False
        input_n += 1    # increment input index
        
    elif sys.argv[input_n] == 'build': # delete any previously installed versions and rebuild all external functions
        cleanFlag = True        
        buildFlag = True
        input_n += 1    # increment input index
        
    elif sys.argv[input_n] == 'help': # print the help
        print 'usage: python install.py [option1] ... [optionN]'
        print '       by default if any previous version is installed newer versions are overwritten but not rebuilt, except if source files have changed.'
        print '\nOptions:'
        print '   clean     : delete any previously built files'
        print '   build     : delete any previously built files and rebuild'
        input_n += 1    # increment input index
        
    else:
        print str(sys.argv[input_n])
        input_n += 1 # increment two indices since this input has two items
        
    if input_n >= len(sys.argv): # if all the inputs have been scanned
        input_read = False   # stop reading them

if len(sys.argv) == 1: # if no input is given just do install
    cleanFlag = True
    buildFlag = True




class BuildError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


# define directories
srcPath = os.path.abspath(os.path.curdir)
buildPath = os.path.join(srcPath,os.pardir,'build')
pythonBuildPath = os.path.join(buildPath,'python')
c_cppBuildPath = os.path.join(buildPath,'c_cpp')
pythonSourcePath = os.path.join(srcPath,'python')
c_cppSourcePath = os.path.join(srcPath,'c_cpp')

c_cpp_cpu_build_file = 'libfmm2d_cpu.so'
c_cpp_gpu_build_file = 'libfmm2d_gpu.so'
python_cpu_build_file = '_fmm2d_py_cpu.so'
python_gpu_build_file = '_fmm2d_py_gpu.so' 

logPath = os.path.join(buildPath,'log')

# create the directory structure for the built files
if not os.path.isdir(pythonBuildPath):
    os.makedirs(pythonBuildPath)
if not os.path.isdir(c_cppBuildPath):
    os.makedirs(c_cppBuildPath)
if not os.path.isdir(logPath):
    os.makedirs(logPath)

logfileName = os.path.join(logPath,build_ID)
log_file = open(logfileName, "w+")

#--------------------------------------------------------------------------------------
# build c/cpp library and move it to build dir
os.chdir(c_cppSourcePath)

# make cpu and copy to build directory
print '\n-- libfmm2d c/cpp --------------------------------------'
if cleanFlag:
    print '   cleaning all...'
    outputMake = commands.getoutput('make clean')
    # dump output of Make to the log file
    log_file.write("%s" % outputMake)

if buildFlag:
    print '   building cpu...'
    outputMake = commands.getoutput('make all')
    # dump output of Make to the log file
    log_file.write("\n\n\n=============================================================")
    log_file.write("libfmm2d c/cpp cpu\n\n")
    log_file.write("%s" % outputMake)
    
    print '   copying cpu...'
    if os.path.isfile(c_cpp_cpu_build_file): # if the build was successful
        shutil.copy(os.path.join(os.curdir,'libfmm2d_cpu.so'),c_cppBuildPath)
    else:
        raise BuildError('Unable to build libfmm2d for c_cpp on cpu')

    # make gpu and copy to build directory
    print '   building gpu...'
    outputMake = commands.getoutput('make all CUDA_DEFS=-DCUDASUPPORT')
    log_file.write("\n\n\n=============================================================")
    log_file.write("libfmm2d c/cpp gpu\n\n")
    log_file.write("%s" % outputMake)

    print '   copying gpu...'
    if os.path.isfile(c_cpp_gpu_build_file): # if the build was successful                                             
        shutil.copy(os.path.join(os.curdir,'libfmm2d_gpu.so'),c_cppBuildPath)
    else:
        raise BuildError('Unable to build libfmm2d for c_cpp on gpu')

print '-- done ------------------------------------------------'

#--------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------------                               
# build python library and move it to build dir                                                                        

os.chdir(pythonSourcePath)

print '\n-- libfmm2d python -------------------------------------'

if cleanFlag:
    print '   cleaning all...'
    outputMake = commands.getoutput('make clean')
    # dump output of Make to the log file
    log_file.write("\n\n\n=============================================================")
    log_file.write("libfmm2d python cpu\n\n")
    log_file.write("%s" % outputMake)

if buildFlag:
    # make cpu and copy to build directory                                                                             
    print '   building cpu...'
    outputMake = commands.getoutput('make all')
    # dump output of Make to the log file
    log_file.write("%s" % outputMake)

    print '   copying cpu...'

    if os.path.isfile(python_cpu_build_file): # if the build was successful                                            
        shutil.copy(os.path.join(os.curdir,'_fmm2d_py_cpu.so'),pythonBuildPath)
    else:
        raise BuildError('Unable to build libfmm2d for python on cpu')

    # make gpu and copy to build directory
    print '   building gpu...'
    outputMake = commands.getoutput('make all CUDA_DEFS=-DCUDASUPPORT')
    # dump output of Make to the log file
    log_file.write("\n\n\n=============================================================")
    log_file.write("libfmm2d python gpu\n\n")
    log_file.write("%s" % outputMake)
    
    print '   copying gpu...'

    if os.path.isfile(python_gpu_build_file): # if the build was successful                                            
        shutil.copy(os.path.join(os.curdir,'_fmm2d_py_gpu.so'),pythonBuildPath)
    else:
        raise BuildError('Unable to build libfmm2d for python on gpu')

print '-- done ------------------------------------------------\n'

#--------------------------------------------------------------------------------------

log_file.close()
