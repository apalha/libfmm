void fmm2d(std::vector<double>& xBlob, std::vector<double>& yBlob, std::vector<double>& wBlob,
                          std::vector<double>& xTarget, std::vector<double>& yTarget,
                          int Ndirect, double xopt, double cutoff, double tol, SMOOTHER smooth,    // the inputs end here
                          std::vector<double>& vx, std::vector<double>& vy);
