/* fmm2d_py.cpp -- Fast multipole method for 2-D potentials in free space
   (Python/C++/CUDA-version) */

/* A. Palha 2013-10-08 (Python/C++ interface, no use of Matlab)*/
/* S. Engblom 2011-06-28 (Panel+Timing I/O) */
/* S. Engblom and A. Goude 2011-04-12 (Major revision) */
/* A. Goude 2010-01-01 (Panels, port to CUDA/GPU) */
/* S. Engblom 2009-05-08 (Revision) */
/* S. Engblom 2007-07-08 (Major revision) */
/* S. Engblom 2006-10-11 (Port to Mex and a major revision) */
/* S. Engblom 2005-01-05 (original code 'fmm2dlp' in C99) */

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <math.h>   //C standard basic mathematical operations
#include <stdlib.h>  //C standard general utilities library
#include <stdio.h>
#include <string.h> //C standard constant and function declaration library
#include <float.h>
using namespace std;

typedef enum {NDIRECT = 0,PANEL = 1,ETA = 2,SORTLIMITS = 3,CUTOFF = 4,
              PRINTTIME = 6,CONT = 7,TOL = 9,
              OUT = 10,CHANNELHEIGHT = 11,SMOOTH = 12,POT = 13,XOPT = 14,
              ERR_PROPERTY = -1} PROPERTY;

#include "fmm.h"
#include "panel.h"

#ifndef CUDASUPPORT
#include "direct.h"
#else
#include "cudaeval.h"
#endif
#ifdef CHANNELPOT
#include "directchannelpot.h"
#include "channelpotpanel.h"
#endif


#ifdef CUDASUPPORT
static int initialized=0;
#endif

void fmm2d(std::vector<double>& xBlob, std::vector<double>& yBlob, std::vector<double>& wBlob,
                          std::vector<double>& xTarget, std::vector<double>& yTarget,
                          int Ndirect, double xopt, double cutoff, double tol, SMOOTHER smooth,    // the inputs end here
                          std::vector<double>& vx, std::vector<double>& vy){                       // the outputs start here

    /*
        FMM calculator of induced velocities given a set of vortex blobs.

        Usage
        -----
            fmm2d(xBlob,yBlob,wBlob,xTarget,yTarget,Ndirect,xopt,cutoff,tol,smooth,vx,vy)

        Parameters
        ----------
            INPUTS:

            xBlob :: the x coordinates of the vortex blobs.
                     (type: std::vector<double>; shape: (nBlobs,1))

            yBlob :: the y coordinates of the vortex blobs.
                     (type: std::vector<double>; shape: (nBlobs,1))

            wBlob :: the circulation associated to the vortex blobs.
                     (type: std::vector<double>; shape: (nBlobs,1))

            xTarget :: the x coordinates of the target points, where to compute induced velocities.
                       (type: std::vector<double>; shape: (nTargets,1))

            yTarget :: the y coordinates of the target points, where to compute induced velocities.
                       (type: std::vector<double>; shape: (nTargets,1))

            Ndirect :: the number of neighbor blobs where to use direct calculation of induced velocities.
                       (type: int; shape: single value)

            xopt :: the core size of the vortices.
                    (type: double; shape: single value)

            cutoff :: the threshold after which to consider 1/r^2 decay of vorticity.
                      typically :: cutoff = 5.0*xopt
                      (type: double; shape: single value)

            tol :: the tolerance used to compute the velocities with FMM, if tol == 0 then direct calculation is performed

            OUTPUTS:

            vx :: the x component of the computed velocities.
                  (type: std::vector<double>; shape: (nTargets,1))

            vy :: the y component of the computed velocities.
                  (type: std::vector<double>; shape: (nTargets,1))


        First added:     2013-11-15

        Copyright (C) 2013 Artur Palha
    */

    /*
        Reviews:
    */

    // c arrays to pass input c++ arrays to c functions
    const double *xBlob_c,*yBlob_c,*wBlob_c,*xTarget_c,*yTarget_c;
    double *vx_c, *vy_c;

    // default values of FMM properties
    bool cont = true;
    double channelheight=-1;
    //SMOOTHER smooth = OSEEN; // Oseen vortex
    float sortlimits[]={2,2,1,1,0,0};
    double eta[]={2,2};
    int pot = 1;
    double *timing = NULL;
    double *wBLob_c_i = NULL;
    double *vx_blob_c = NULL;
    double *vy_blob_c = NULL;
    bool printtime = false;
    panel *panels = NULL;
    int nPanels = 0;

    // initialize the number of blobs and targets
    int nBlobs, nTargets;

    // compute the number of targets, it is assumed that yTarget and xTarget have the same size
    nTargets = xTarget.size();

    // compute the number of blobs, it is assumed that yBlob and xBlob have the same size
    nBlobs = xBlob.size();

    // extract pointers to the inputs and outputs

    // blobs x and y coordinates and circulation
    xBlob_c = &xBlob[0];
    yBlob_c = &yBlob[0];
    wBlob_c = &wBlob[0];

    // targets x and y coordinates
    xTarget_c = &xTarget[0];
    yTarget_c = &yTarget[0];

    // x and y components of induced velocities
    vx_c = &vx[0];
    vy_c = &vy[0];

    // compute the fmm2d velocity

    if (tol == 0.0){ // if tol == 0.0 then we must use direct method to compute the induced velocities
	#ifdef CUDASUPPORT
	direct_eval_cuda(nBlobs,xBlob_c,yBlob_c,wBlob_c,wBLob_c_i,nTargets,xTarget_c,yTarget_c,vx_blob_c,vy_blob_c,vy_c,vx_c,panels,nPanels, // vx_c is swapped with vx_y because internally it uses imaginary representation
                         pot,smooth,xopt,cutoff,cont,timing,printtime);                                                                  // v = - 2*pi*vy_c - 2*pi*vx_c i
	#else
	directInteract(nBlobs,xBlob_c,yBlob_c,wBlob_c,wBLob_c_i,nTargets,xTarget_c,yTarget_c,vx_blob_c,vy_blob_c,vy_c,vx_c,panels,nPanels,   // vx_c is swapped with vx_y because internally it uses imaginary representation
                   pot,smooth,xopt,cutoff,cont,timing,printtime);                                                                        // v = - 2*pi*vy_c - 2*pi*vx_c i
	#endif
    }
    else { // if tol > 0.0 then we use fmm to compute the induced velocities
        fmm2dInteract(nBlobs,xBlob_c,yBlob_c,wBlob_c,wBLob_c_i,nTargets,xTarget_c,yTarget_c,vx_blob_c,vy_blob_c,vy_c,vx_c,panels,nPanels, // vx_c is swapped with vx_y because internally it uses imaginary representation
                  pot,tol,Ndirect,smooth,xopt,cutoff,cont,timing,                                                                         // v = - 2*pi*vy_c - 2*pi*vx_c i
                  printtime,sortlimits,eta,channelheight);
    }

    // Now since v = - 2*pi*vy_c - 2*pi*vx_c i, we need to multiply both velocities by -1/(2*pi)
    std::transform(vx.begin(), vx.end(), vx.begin(), std::bind1st(std::multiplies<double>(),-1.0/(2.0*M_PI)));
    std::transform(vy.begin(), vy.end(), vy.begin(), std::bind1st(std::multiplies<double>(),-1.0/(2.0*M_PI)));

}


